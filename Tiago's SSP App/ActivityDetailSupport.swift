//
//  ActivityDetailSupport.swift
//  ActivityWallet
//
//  Created by Tassos on 27/07/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation



struct SigninButton: View {
    var activity: Activity
    var body: some View {
        ZStack {
            Rectangle()
                .frame(minWidth: 25, idealWidth: 162, maxWidth: 162, minHeight: 25, idealHeight: 60, maxHeight: 60, alignment: .center)
                .cornerRadius(10)
                .foregroundColor(Color(uiColor: UIColor.systemGray4))
                VStack(alignment: .center, spacing:6) {
                    Image(systemName: "bookmark.fill").font(.system(size: 22))
                    Text("Sign Up").font(.system(size: 11)).padding(.top, 2)
                   
                } .foregroundColor(activity.labelColors[0]).padding(.top, 11).padding(.bottom, 10)
        }
        
    }
    
}
struct CheckinButton: View {
    var activity: Activity
    var body: some View {
        ZStack {
            Rectangle()
                .frame(minWidth: 25, idealWidth: 162, maxWidth: 162, minHeight: 25, idealHeight: 60, maxHeight: 60, alignment: .center)
                .cornerRadius(10)
                .foregroundColor(Color(uiColor: UIColor.systemGray4))
                VStack(alignment: .center, spacing:6) {
                                   Image(systemName: "checkmark.seal.fill").font(.system(size: 22))
                                   Text("Check In").font(.system(size: 11)).padding(.top, 2)
                                  
                               } .foregroundColor(activity.labelColors[0]).padding(.top, 11).padding(.bottom, 10)
        }
        
    }
    
}

//#if DEBUG
struct ActivityDetail_Previews: PreviewProvider {
    static var previews: some View {
      //  ActivityDetailExperiment(activity: activityData[0])
        Text("F")
    }
}
//#endif

extension UIColor {
    var rgba: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        return (red, green, blue, alpha)
    }
}

extension Color {
    init(uiColor: UIColor) {
        self.init(red: Double(uiColor.rgba.red),
                  green: Double(uiColor.rgba.green),
                  blue: Double(uiColor.rgba.blue),
                  opacity: Double(uiColor.rgba.alpha))
    }
}
