//
//  ActivityList.swift
//  ActivityWallet
//
//  Created by Tassos on 24/07/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import SwiftUI
import CoreData
struct ActivityList: View {

    @Environment(\.managedObjectContext) var viewContext
    @FetchRequest(fetchRequest: Activity.allActivitiesFetchRequest()) var activityStore: FetchedResults<Activity>
    //var store: [Activity] = []
    
    var body: some View {
        NavigationView {
            ScrollView {
                //store = activityStore
                ForEach(activityStore) { activity in
                    NavigationLink(destination:
                        ActivityDetailForm(activity: activity)
                    ) {
                        ActivityCard(activity: activity)
                            .padding(.top, 5).padding(.bottom, 5)
                            .padding(.leading, 18).padding(.trailing, 18)
                            .frame(idealHeight: 130, alignment: .leading)
                    }
                }
                
            }
                
            .navigationBarTitle("My Activities", displayMode: .large)
        }
    }
}

struct ActivityListView_Previews: PreviewProvider {
    static var previews: some View {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return   ForEach(["iPhone XS",
        //                 "iPhone SE"
        ], id: \.self) { deviceName in
            ActivityList().environment(\.managedObjectContext, context)
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
    
}


