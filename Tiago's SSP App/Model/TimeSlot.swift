//
//  TimeSlot.swift
//  ActivityWallet
//
//  Created by Tassos on 16/09/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation
import CoreData
import SwiftUI

let defaultDate = Date(timeIntervalSince1970: TimeInterval(0))

extension TimeSlot {
    //computing Date objects based on the parsed strings and ints
    var startTime: Date { (formatter.date(from: self.startDateString!) ?? defaultDate)! }
    var timeSlot: DateInterval { DateInterval(start: self.startTime, duration: TimeInterval(self.durationInt))}
    var endTime: Date { timeSlot.end  }
    
    var signupStartTime: Date { (formatter.date(from: self.signupDateString!) ?? defaultDate)! }
    var signupSlot: DateInterval { DateInterval(start: self.signupStartTime, duration: TimeInterval(self.signupSlotDurationInt))}
    var signupEnd: Date { signupSlot.end }
    
    //signup is open if current date is in the signup slot
    var isSignupOpen: Bool { signupSlot.contains(Date()) }

}

extension TimeSlot {
    static func allTimeSlotsFetchRequest() -> NSFetchRequest<TimeSlot> {
        let request: NSFetchRequest<TimeSlot> = TimeSlot.fetchRequest()
        
        request.sortDescriptors = [NSSortDescriptor(key: "signupDateString", ascending: true)]
          
        return request
    }
}

extension TimeSlot {

    enum timeslotForActivityFormats {
        case long
        case short
    }
    static func getDateStrings(_ cloudkitSlots: NSSet, format: timeslotForActivityFormats) -> [String] {
        if cloudkitSlots.count == 0 { return [""] }
        let timeSlots = {Array(cloudkitSlots) as! [TimeSlot]}().sorted(by: {
            return $0 < $1
        })
        let formatter = DateFormatter()
        var dateStrings: [String] = []
        for slot in timeSlots {
            switch format {
            case .long:
                formatter.dateFormat = "EEEE dd MMMM HH:mm"
            case .short:
                formatter.dateFormat = "HH:mm"
            }
            
            
            let first = formatter.string(from: slot.timeSlot.start)
            formatter.dateFormat = "HH:mm"
            let second = formatter.string(from: slot.timeSlot.end)
            dateStrings.append(first + " - " + second)
        }
        
        return dateStrings
    }
}

extension TimeSlot: Comparable {
    public static func < (lhs: TimeSlot, rhs: TimeSlot) -> Bool {
        return lhs.startTime < rhs.startTime
    }
    
    
}


