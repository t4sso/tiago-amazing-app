//
//  ActivityTimeSlot.swift
//  ActivityWallet
//
//  Created by Tassos on 27/07/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation
import SwiftUI

struct ActivitySlot: Hashable, Codable, Identifiable {
    var id: Int
    var capacity: Int
    //Dates will be parsed from whatever json/cloudkit as string and int.
    var startDateString: String = "2010-06-19T18:43:19Z"
    var durationInt: Int = 3600
    var signupDateString: String = "2008-06-19T18:43:19Z"
    var signupSlotDurationInt: Int = 3600
    
    
    //computing Date objects based on the parsed strings and ints
    let defaultDate = Date(timeIntervalSince1970: TimeInterval(0))
    var startTime: Date { (formatter.date(from: startDateString) ?? defaultDate)! }
    var timeSlot: DateInterval { DateInterval(start: startTime, duration: TimeInterval(durationInt))}
    var endTime: Date { timeSlot.end  }
    
    var signupStartTime: Date { (formatter.date(from: signupDateString) ?? defaultDate)! }
    var signupSlot: DateInterval { DateInterval(start: signupStartTime, duration: TimeInterval(signupSlotDurationInt))}
    var signupEnd: Date { signupSlot.end }
    
    //signup is open if current date is in the signup slot
    var isSignupOpen: Bool { signupSlot.contains(Date()) }

}
let defSlot = ActivitySlot(id: 1001, capacity: 9, startDateString: "2010-06-19T18:43:19Z", durationInt: 3600, signupDateString: "2008-06-19T18:43:19Z", signupSlotDurationInt: 3600)

