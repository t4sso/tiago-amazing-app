//
//  Activity.swift
//  ActivityWallet
//
//  Created by Tassos on 12/09/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation
import CoreData
import SwiftUI

extension Activity: Identifiable  {

    var type: Category  {
        switch self.category {
        case "Session":
            return .session
        case "Lab":
            return .lab
        case "Workshop":
            return .workshop
        case "Meet and Share":
            return .meetnshare
        case "Consultation":
            return .consultation
        case "Custom":
            return .custom
        default:
            return .session
        }
    }
    enum Category: String, CaseIterable, Codable, Hashable {
        case session = "Session"
        case lab = "Lab"
        case workshop = "Workshop"
        case meetnshare = "Meet and Share"
        case consultation = "Consultation"
        case custom = "Custom"
        
    }

    enum Location: String, CaseIterable, Codable, Hashable {
     
        case main = "Main Classroom"
        case auditorium = "Auditorium"
        
        case br1 = "Boardroom 1"
        case br2 = "Boardroom 2"
        case br3 = "Boardroom 3"
        
        case lab1 = "Lab 1"
        case lab2 = "Lab 2"
        case lab3 = "Lab 3"
        case lab4 = "Lab 4"
        
        case lab01_01 = "Lab 01 - 01"
        case lab01_02 = "Lab 01 - 02"
        case lab01_03 = "Lab 01 - 03"
        case lab01_04 = "Lab 01 - 04"
        case lab01_05 = "Lab 01 - 05"
        case lab01_06 = "Lab 01 - 06"
        
        case lab02_01 = "Lab 02 - 01"
        case lab02_02 = "Lab 02 - 02"
        case lab02_03 = "Lab 02 - 03"
        case lab02_04 = "Lab 02 - 04"
        
        case lab03_01 = "Lab 03 - 01"
        case lab03_02 = "Lab 03 - 02"
        case lab03_03 = "Lab 03 - 03"
        case lab03_04 = "Lab 03 - 04"
        case lab03_05 = "Lab 03 - 05"
        case lab03_06 = "Lab 03 - 06"
        case lab03_07 = "Lab 03 - 07"
        case lab03_08 = "Lab 03 - 08"

        case lab04_01 = "Lab 04 - 01"
        case lab04_02 = "Lab 04 - 02"
        case lab04_03 = "Lab 04 - 03"
        case lab04_04 = "Lab 04 - 04"
        
        case collab1 = "Collab 1"
        case collab2 = "Collab 2"
        case collab3 = "Collab 3"
        case collab4 = "Collab 4"
        
        case collab01_01 = "Collab 01 - 01"
        case collab01_02 = "Collab 01 - 02"
        case collab01_03 = "Collab 01 - 03"
        case collab01_04 = "Collab 01 - 04"
        
        case collab02_01 = "Collab 02 - 01"
        case collab02_02 = "Collab 02 - 02"
        case collab02_03 = "Collab 02 - 03"
        case collab02_04 = "Collab 02 - 04"
        case collab02_05 = "Collab 02 - 05"
        case collab02_06 = "Collab 02 - 06"
        
        case collab03_01 = "Collab 03 - 01"
        case collab03_02 = "Collab 03 - 02"
        case collab03_03 = "Collab 03 - 03"
        case collab03_04 = "Collab 03 - 04"
        case collab03_05 = "Collab 03 - 05"
        case collab03_06 = "Collab 03 - 06"
        
        case collab04_01 = "Collab 04 - 01"
        case collab04_02 = "Collab 04 - 02"
        case collab04_03 = "Collab 04 - 03"
        case collab04_04 = "Collab 04 - 04"
    }
    
    
    var gradientColors:[Color] {
        switch self.type {
            
            
        case .session:
            return [Color(red: 255/255, green: 149/255, blue: 0/255), Color(red: 255/255, green: 199/255, blue: 0/255)]
        case .lab:
            return [Color(red: 0/255, green: 122/255, blue: 255/255), Color(red: 0/255, green: 178/255, blue: 255/255)]
        case .workshop:
            return [Color(red: 88/255, green: 86/255, blue: 214/255), Color(red: 145/255, green: 143/255, blue: 237/255)]
        case .custom:
            return [Color(red: 90/255, green: 200/255, blue: 250/255), Color(red: 147/255, green: 230/255, blue: 253/255)]
        case .meetnshare:
            return [Color(red: 255/255, green: 45/255, blue: 85/255), Color(red: 255/255, green: 89/255, blue: 142/255)]
        case .consultation:
            return [Color(red: 52/255, green: 199/255, blue: 89/255), Color(red: 100/255, green: 229/255, blue: 146/255)]
    
        }
    }
    var labelColors:[Color] {
       switch self.type {
            
            
        case .session:
            return [Color(red: 255/255, green: 149/255, blue: 0/255), Color(red: 255/255, green: 199/255, blue: 0/255)]
        case .lab:
            return [Color(red: 0/255, green: 122/255, blue: 255/255), Color(red: 0/255, green: 178/255, blue: 255/255)]
        case .workshop:
            return [Color(red: 88/255, green: 86/255, blue: 214/255), Color(red: 145/255, green: 143/255, blue: 237/255)]
        case .custom:
            return [Color(red: 90/255, green: 200/255, blue: 250/255), Color(red: 147/255, green: 230/255, blue: 253/255)]
        case .meetnshare:
            return [Color(red: 255/255, green: 45/255, blue: 85/255), Color(red: 255/255, green: 89/255, blue: 142/255)]
        case .consultation:
            return [Color(red: 52/255, green: 199/255, blue: 89/255), Color(red: 100/255, green: 229/255, blue: 146/255)]
    
        }
    }
}

extension Activity {
    static func create(in managedObjectContext: NSManagedObjectContext){
        
        //MARK: The below lines were used just to load stuff into CK. ONCE. We can use this to save new stuff on CK.
        //
        //        for activity in activityData {
        //            let newActivity = NSEntityDescription.insertNewObject(forEntityName: "Activity", into: managedObjectContext) as! Activity
        //            newActivity.name = activity.name
        //            newActivity.subtitle = activity.subtitle
        //            newActivity.location = activity.location.rawValue
        //            newActivity.userDidSignup = activity.userDidSignUp
        //            newActivity.userDidCheckIn = activity.userDidCheckIn
        //            newActivity.category = activity.category.rawValue
        //            newActivity.detailsText = activity.detailsText
        //            newActivity.resources = activity.resources?.absoluteString
        //            newActivity.showInCalendar = activity.showInCalendar
        //            newActivity.allowNotifications = activity.allowNotifications
        //            for slot in activity.slot {
        //                let newSlot = NSEntityDescription.insertNewObject(forEntityName: "TimeSlot", into: managedObjectContext) as! TimeSlot
        //                newSlot.capacity = Int16(slot.capacity)
        //                newSlot.durationInt = Int16(slot.durationInt)
        //                newSlot.startDateString = slot.startDateString
        //                newSlot.signupDateString = slot.signupDateString
        //                newSlot.signupSlotDurationInt = Int16(slot.signupSlotDurationInt)
        //                newSlot.parentActivity = newActivity
        //                newActivity.childslots?.addingObjects(from: [newSlot])
        //            }
        //
        //
        //
        //        }
        //
        //        do {
        //                try  managedObjectContext.save()
        //
        //            } catch {
        //                // Replace this implementation with code to handle the error appropriately.
        //                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //                let nserror = error as NSError
        //                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        //            }
    }
}


extension Activity {
    static func allActivitiesFetchRequest() -> NSFetchRequest<Activity> {
        let request: NSFetchRequest<Activity> = Activity.fetchRequest() //as! NSFetchRequest<Activity>
        
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
          
        return request
    }
}


    extension Collection where Element == Activity, Index == Int {
        func delete(at indices: IndexSet, from managedObjectContext: NSManagedObjectContext) {
            indices.forEach { managedObjectContext.delete(self[$0]) }
     
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
}

extension Activity {
    
    static func createPreviewData(in managedObjectContext: NSManagedObjectContext) -> [Activity] {
        
        var previewData: [Activity] = []
        
        let newActivity = NSEntityDescription.insertNewObject(forEntityName: "Activity", into: managedObjectContext) as! Activity

        newActivity.subtitle = "Steve Jobs, Apple"
        newActivity.location = "Auditorium"
        newActivity.userDidSignup = false
        newActivity.userDidCheckIn = false
        newActivity.category = "Workshop"
        newActivity.detailsText = "WZPN opens up opportunities for creating new and engaging experiences. Sketch is a new framework chich you can use to easily integrate Color models into your app. See how Adobe and Jony Ive can help you make your app more intelligent with just a few lines of code."
        newActivity.showInCalendar = false
        newActivity.allowNotifications = false
        let newSlot = NSEntityDescription.insertNewObject(forEntityName: "TimeSlot", into: managedObjectContext) as! TimeSlot
                        newSlot.capacity = Int16(9)
                        newSlot.durationInt = Int16(3600)
                        newSlot.startDateString = "2019-07-22T13:00:00Z"
                        newSlot.signupDateString = "2019-07-21T09:30:00Z"
                        newSlot.signupSlotDurationInt = Int16(10801)
                        newSlot.parentActivity = newActivity
                        newActivity.childslots?.addingObjects(from: [newSlot])

        previewData.append(newActivity)
        
        let newActivity2 = NSEntityDescription.insertNewObject(forEntityName: "Activity", into: managedObjectContext) as! Activity
        
        newActivity2.subtitle = "Introduction to CoreML"
        newActivity2.location = "Main Classroom"
        newActivity2.userDidSignup = false
        newActivity2.userDidCheckIn = false
        newActivity2.category = "Session"
        newActivity2.detailsText = "Machine Learning opens up opportunities for creating new and engaging experiences. CoreML is a new framework chich you can use to easily integrate machine learning models into your app. See how Xcode and CoreML can help you make your app more intelligent with just a few lines of code."
        newActivity2.showInCalendar = true
        newActivity2.allowNotifications = true
        let newSlot2 = NSEntityDescription.insertNewObject(forEntityName: "TimeSlot", into: managedObjectContext) as! TimeSlot
                        newSlot2.capacity = Int16(9)
                        newSlot2.durationInt = Int16(7200)
                        newSlot2.startDateString = "2019-07-18T14:00:00Z"
                        newSlot2.signupDateString = "2018-07-18T14:00:00Z"
                        newSlot2.signupSlotDurationInt = Int16(10801)
                        newSlot2.parentActivity = newActivity2
                        newActivity2.childslots?.addingObjects(from: [newSlot2])
        let newSlot3 = NSEntityDescription.insertNewObject(forEntityName: "TimeSlot", into: managedObjectContext) as! TimeSlot
        newSlot3.capacity = Int16(20)
        newSlot3.durationInt = Int16(7200)
        newSlot3.startDateString = "2019-08-01T14:00:00Z"
        newSlot3.signupDateString = "2018-07-18T14:00:00Z"
        newSlot3.signupSlotDurationInt = Int16(10801)
        newSlot3.parentActivity = newActivity2
        newActivity2.childslots?.addingObjects(from: [newSlot3])
        previewData.append(newActivity2)
        
        
        return previewData
    }
    
}
