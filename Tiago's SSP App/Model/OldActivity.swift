//
//  Activity.swift
//  ActivityWallet
//
//  Created by Tassos on 24/07/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation
import SwiftUI

enum Category: String, CaseIterable, Codable, Hashable {
    case session = "Session"
    case lab = "Lab"
    case workshop = "Workshop"
    case meetnshare = "Meet and Share"
    case consultation = "Consultation"
    case custom = "Custom"
    
}

enum Location: String, CaseIterable, Codable, Hashable {
 
    case main = "Main Classroom"
    case auditorium = "Auditorium"
    
    case br1 = "Boardroom 1"
    case br2 = "Boardroom 2"
    case br3 = "Boardroom 3"
    
    case lab1 = "Lab 1"
    case lab2 = "Lab 2"
    case lab3 = "Lab 3"
    case lab4 = "Lab 4"
    
    case lab01_01 = "Lab 01 - 01"
    case lab01_02 = "Lab 01 - 02"
    case lab01_03 = "Lab 01 - 03"
    case lab01_04 = "Lab 01 - 04"
    case lab01_05 = "Lab 01 - 05"
    case lab01_06 = "Lab 01 - 06"
    
    case lab02_01 = "Lab 02 - 01"
    case lab02_02 = "Lab 02 - 02"
    case lab02_03 = "Lab 02 - 03"
    case lab02_04 = "Lab 02 - 04"
    
    case lab03_01 = "Lab 03 - 01"
    case lab03_02 = "Lab 03 - 02"
    case lab03_03 = "Lab 03 - 03"
    case lab03_04 = "Lab 03 - 04"
    case lab03_05 = "Lab 03 - 05"
    case lab03_06 = "Lab 03 - 06"
    case lab03_07 = "Lab 03 - 07"
    case lab03_08 = "Lab 03 - 08"

    case lab04_01 = "Lab 04 - 01"
    case lab04_02 = "Lab 04 - 02"
    case lab04_03 = "Lab 04 - 03"
    case lab04_04 = "Lab 04 - 04"
    
    case collab1 = "Collab 1"
    case collab2 = "Collab 2"
    case collab3 = "Collab 3"
    case collab4 = "Collab 4"
    
    case collab01_01 = "Collab 01 - 01"
    case collab01_02 = "Collab 01 - 02"
    case collab01_03 = "Collab 01 - 03"
    case collab01_04 = "Collab 01 - 04"
    
    case collab02_01 = "Collab 02 - 01"
    case collab02_02 = "Collab 02 - 02"
    case collab02_03 = "Collab 02 - 03"
    case collab02_04 = "Collab 02 - 04"
    case collab02_05 = "Collab 02 - 05"
    case collab02_06 = "Collab 02 - 06"
    
    case collab03_01 = "Collab 03 - 01"
    case collab03_02 = "Collab 03 - 02"
    case collab03_03 = "Collab 03 - 03"
    case collab03_04 = "Collab 03 - 04"
    case collab03_05 = "Collab 03 - 05"
    case collab03_06 = "Collab 03 - 06"
    
    case collab04_01 = "Collab 04 - 01"
    case collab04_02 = "Collab 04 - 02"
    case collab04_03 = "Collab 04 - 03"
    case collab04_04 = "Collab 04 - 04"
}

struct OldActivity: Hashable, Codable, Identifiable {

    
    var id: Int
    var name: String
    var subtitle: String?
    var location: Location
    var userDidSignUp: Bool
    var userDidCheckIn: Bool
    var category: Category
    var detailsText: String
    var resources: URL?
    var showInCalendar: Bool
    var allowNotifications: Bool
    
    var slot: [ActivitySlot] = [defSlot]
   
    var gradientColors:[Color] {
        switch self.category {
            
            
        case .session:
            return [Color(red: 255/255, green: 149/255, blue: 0/255), Color(red: 255/255, green: 199/255, blue: 0/255)]
        case .lab:
            return [Color(red: 0/255, green: 122/255, blue: 255/255), Color(red: 0/255, green: 178/255, blue: 255/255)]
        case .workshop:
            return [Color(red: 88/255, green: 86/255, blue: 214/255), Color(red: 145/255, green: 143/255, blue: 237/255)]
        case .custom:
            return [Color(red: 90/255, green: 200/255, blue: 250/255), Color(red: 147/255, green: 230/255, blue: 253/255)]
        case .meetnshare:
            return [Color(red: 255/255, green: 45/255, blue: 85/255), Color(red: 255/255, green: 89/255, blue: 142/255)]
        case .consultation:
            return [Color(red: 52/255, green: 199/255, blue: 89/255), Color(red: 100/255, green: 229/255, blue: 146/255)]
    
            
            // OLD:
            //        case .design:
            //            return [Color(red: 255/255, green: 149/255, blue: 0/255), Color(red: 255/255, green: 199/255, blue: 0/255)]
            //        case .coding:
            //            return [Color(red: 0/255, green: 122/255, blue: 255/255
            ////                , opacity: 0
            //                ), Color(red: 0/255, green: 178/255, blue: 255/255
            ////                , opacity: 0.2
            //                )]
            //        case .inspirational:
            //           return [Color(red: 88/255, green: 86/255, blue: 214/255), Color(red: 145/255, green: 143/255, blue: 237/255)]
            //        case .community:
            //           return [Color(red: 255/255, green: 59/255, blue: 48/255), Color(red: 255/255, green: 109/255, blue: 94/255)]
            //        case .hybrid:
            //           return [Color(red: 175/255, green: 82/255, blue: 222/255), Color(red: 215/255, green: 138/255, blue: 241/255)]
            //        case .external:
            //           return [Color(red: 90/255, green: 200/255, blue: 250/255), Color(red: 147/255, green: 230/255, blue: 253/255)]
            //        case .pier:
            //           return [Color(red: 255/255, green: 45/255, blue: 85/255), Color(red: 255/255, green: 89/255, blue: 142/255)]
            //        case .consultation:
            //           return [Color(red: 255/255, green: 204/255, blue: 0/255), Color(red: 255/255, green: 232/255, blue: 0/255)]
            //        case .learning:
            //           return [Color(red: 52/255, green: 199/255, blue: 89/255), Color(red: 100/255, green: 229/255, blue: 146/255)]
            //        default:
            //            return [Color.red, Color.blue]
        }
    }
    var labelColors:[Color] {
       switch self.category {
            
            
        case .session:
            return [Color(red: 255/255, green: 149/255, blue: 0/255), Color(red: 255/255, green: 199/255, blue: 0/255)]
        case .lab:
            return [Color(red: 0/255, green: 122/255, blue: 255/255), Color(red: 0/255, green: 178/255, blue: 255/255)]
        case .workshop:
            return [Color(red: 88/255, green: 86/255, blue: 214/255), Color(red: 145/255, green: 143/255, blue: 237/255)]
        case .custom:
            return [Color(red: 90/255, green: 200/255, blue: 250/255), Color(red: 147/255, green: 230/255, blue: 253/255)]
        case .meetnshare:
            return [Color(red: 255/255, green: 45/255, blue: 85/255), Color(red: 255/255, green: 89/255, blue: 142/255)]
        case .consultation:
            return [Color(red: 52/255, green: 199/255, blue: 89/255), Color(red: 100/255, green: 229/255, blue: 146/255)]
    
        }
    }
}

let formatter = ISO8601DateFormatter()

