//
//  ActivitySchedule.swift
//  ActivityWallet
//
//  Created by Fabrizio Casaburi on 11/09/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import SwiftUI

struct ActivitySchedule: View {
     @Environment(\.managedObjectContext) var viewContext
    @FetchRequest(fetchRequest: Activity.allActivitiesFetchRequest()) var activityStore: FetchedResults<Activity>

    var body: some View {
        NavigationView {
            ScrollView {
                
                //DAY
                VStack(alignment: HorizontalAlignment.leading, spacing: 20) {
                    FormSectionHeader(text: "TODAY")
                    
                    //ACTIVITIES CELL
                    VStack(alignment: .leading, spacing: 14) {
                        
                        ForEach(0..<activityStore.count) { counter in
                            
                            
                            FormFieldMultiLineCell(activity: self.activityStore[counter], cellIndex: counter, cellTotalNumber: self.activityStore.count)
                            
                        }
                        
                    }
                    .foregroundColor(.black)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.white))
                    
                    
                    
                }
                .padding(.horizontal, 20)
                .padding(.bottom, 20)
                .padding(.top, 19) //WORKAROUND: Should be 20
            }
            .padding(.top, 1) //WORKAROUND: It shouldn't be needed but apparently, without it, the Scroll View scrolls over the Nav Bar unless the Nav Bar Title is inline
            .background(Color(.systemGray6))
            .navigationBarItems(
                leading: Button(
                    action: { /*Go to Today*/ },
                    label: { Text("Today")}
                ),
                trailing: Button(
                    action: { /*Summon Filter Modal View*/ },
                    label: { Text("Filter")}
                )
            )
            .edgesIgnoringSafeArea(.bottom)
            .navigationBarTitle("Schedule", displayMode: .large)
        }
    }
}

struct ActivitySchedule_Previews: PreviewProvider {
    static var previews: some View {
        ActivitySchedule()
    }
}

struct FormFieldMultiLineCell: View {
    @Environment(\.managedObjectContext) var viewContext
      @FetchRequest(fetchRequest: Activity.allActivitiesFetchRequest()) var activityStore: FetchedResults<Activity>
    var activity: Activity
    var cellIndex: Int
    var cellTotalNumber: Int
    var position: FormFieldMultiLineCellPosition {
        var value: FormFieldMultiLineCellPosition = .single
        if cellIndex == 0 && cellTotalNumber != 1 {
            value = .top
        }
        if cellIndex != 0 && cellIndex != cellTotalNumber-1 {
            value = .middle
        }
        if cellIndex == activityStore.count-1 && cellTotalNumber != 1 { value = .bottom
        }
        return value
    }
    
    
    
    var body: some View {
        Group {
            //NOTE: it seems like you can't use switch stataments in ViewBuilders
            if self.position == .top {
                base
                    .padding(.top, 14)
//                    .foregroundColor(.red)
                Divider()
            }
            if self.position == .middle {
                base
//                    .foregroundColor(.blue)
                Divider()
            }
            if self.position == .bottom {
                base
                    .padding(.bottom, 14)
//                    .foregroundColor(.yellow)
            }
            if self.position == .single {
                base
                    .padding(.vertical, 14)
//                    .foregroundColor(.purple)
            }
        }
    }
    
    var base: some View {
        HStack(spacing: 14) {
            VStack(alignment: HorizontalAlignment.leading) {
             
                Text(activity.name ?? "")
                Text("\(TimeSlot.getDateStrings(activity.childslots ?? NSSet(), format: .short)[0]) - \(String(self.activity.location ?? ""))")
                    .font(.subheadline)
                    .foregroundColor(.gray)
                
            }
            .layoutPriority(1)
            Spacer()
            Image(systemName: "bookmark.fill")
                .foregroundColor(.gray)
            Image(systemName: "chevron.right")
                .foregroundColor(.gray)
        }
        .padding(.horizontal, 14)
    }
}

enum FormFieldMultiLineCellPosition {
    case top
    case middle
    case bottom
    case single
}
