//
//  NewCard.swift
//  ActivityWallet
//
//  Created by Tassos on 17/09/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation
import SwiftUI


struct CardPath: View {
    @Environment(\.managedObjectContext) var viewContext
    var activity: Activity
    var body: some View {
        //Marco Falanga is the man
        GeometryReader { geometry in
            Path { path in
                let w = geometry.size.width
                let h = geometry.size.height
                let hToRefer = h * 31/130
                let controlPoint = CGPoint(x: w *  305/335, y: h/2)
                path.move(to: CGPoint.zero)
                path.addLine(to: CGPoint(x: w, y: 0))
                path.addLine(to: CGPoint(x: w, y: hToRefer))
                path.addQuadCurve(to: CGPoint(x: w, y: h - hToRefer), control: controlPoint)
                path.addLine(to: CGPoint(x: w, y: h - (h/4)))
                path.addLine(to: CGPoint(x: w, y: h))
                path.addLine(to: CGPoint(x: 0, y: h))
            }
            .fill(LinearGradient(
                gradient: .init(colors:self.activity.gradientColors),
                startPoint: .init(x: 0, y: 1),
                endPoint: .init(x: 1, y: 0)))
                .clipShape(RoundedRectangle(cornerRadius: 10))
        }
    }
}

struct ActivityCard: View {
    @Environment(\.managedObjectContext) var viewContext
    var activity: Activity
    var body: some View {
        
        
        ZStack (alignment: Alignment.leading) {
            CardPath(activity: activity)
            VStack(alignment: .leading) {
                Text(activity.name!)
                    .font(.system(size: 20))
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .kerning(0.75)
                Text(self.activity.subtitle ?? "")
                    .font(.system(size: 20))
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .kerning(0.75)
                Spacer()
                Text(TimeSlot.getDateStrings(activity.childslots ?? NSSet(), format: .long)[0])
                    .font(.system(size: 13))
                    .foregroundColor(.white)
                    .fontWeight(.regular)
                Text(String(self.activity.location!))
                    .font(.system(size: 13))
                    .foregroundColor(.white)
                    .fontWeight(.regular)
            }
            .padding(.init(top: 10, leading: 16, bottom: 10, trailing: 16))
        }
            
        .frame(idealHeight: 130, alignment: .leading)
        
    }
}


struct NewCardView_Previews: PreviewProvider {
    static var previews: some View {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
            return ForEach(Activity.createPreviewData(in: context)) { activity in
                ActivityCard(activity: activity).environment(\.managedObjectContext,context)
            .previewLayout(.fixed(width: 335, height: 130))
        }}
}

