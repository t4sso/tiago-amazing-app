//
//  DetailForm.swift
//  ActivityWallet
//
//  Created by Tassos on 28/07/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import SwiftUI
import CoreData

struct ActivityDetailForm: View {
    @State var activity: Activity
    @Environment(\.managedObjectContext) var viewContext
       @FetchRequest(fetchRequest: Activity.allActivitiesFetchRequest()) var activityStore: FetchedResults<Activity>
    var body: some View {
        
        ScrollView {
            VStack(spacing: 24) {
                
                //SECTION
                VStack(spacing: 20) {
                    ActivityCard(activity: self.activity)
                        .frame(height: 130)
                    HStack {
                        SigninButton(activity: self.activity)
                        Spacer()
                        CheckinButton(activity: self.activity)
                    }
                    FormSectionFooter(text: "Sign up available until 13:00")
                        .padding(.top, 4)
                }
                
                //SECTION
                VStack(alignment: .leading, spacing: 20) {
                    FormSectionHeader(text: "DETAILS")
                        .font(.subheadline)
                        .padding(.bottom, -14)
                    FormFieldMultiLineText(text: self.activity.detailsText ?? "")
                }
                
                //SECTION
                VStack(alignment: .leading, spacing: 20) {
                    FormSectionHeader(text: "ACTIVITY INFORMATION")
                    FormFieldMultiLineText(text: "Test")
                    FormSectionFooter(text: "3 more slots available")
                }
                
                //SECTION
                VStack(alignment: .leading, spacing: 20) {
                    FormSectionHeader(text: "SETTINGS")
                    
                    VStack(spacing: 6) {
                        Toggle(isOn: self.$activity.showInCalendar) {
                            Text("Show in Calendar")
                        }
                        Divider()
                        Toggle(isOn: self.$activity.allowNotifications) {
                            Text("Allow Notifications")
                                .foregroundColor(.black)
                        }
                    }
                    .foregroundColor(.black)
                    .padding(.horizontal, 16)
                    .padding(.vertical, 6)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.white))
                    
                    Button(action: {
                        //Remove This Activity
                    }) {
                        Text("Remove This Activity")
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.vertical, 11)
                    .padding(.horizontal, 16)
                    .foregroundColor(Color(.red))
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.white))
                    
                }
            }
            .padding(20)
        }
        .background(Color(.systemGray6))
        .navigationBarTitle("", displayMode: .inline)
        .edgesIgnoringSafeArea(.bottom)
    }
}

        
struct FormSectionFooter: View {
    var text: String
    
    var body: some View {
        Text(text)
            .foregroundColor(Color(.systemGray))
            .font(.footnote)
            .padding(.horizontal, 16)
            .padding(.top, -14)
    }
}

struct FormSectionHeader: View {
    var text: String
    
    var body: some View {
        Text(text)
            .foregroundColor(Color(.systemGray))
            .font(.footnote)
            .padding(.horizontal, 16)
            .padding(.bottom, -14)
    }
}

struct FormFieldMultiLineText: View {
    var text: String
    
    var body: some View {
        Text(text)
            .fixedSize(horizontal: false, vertical: true) // WORKAROUND
            .lineLimit(Int.max) // BUG: .lineLimit(nil) doesn't seem to work as of Xcode beta 7
            .frame(maxWidth: .infinity, alignment: .leading)
            .multilineTextAlignment(.leading)
            .padding(.horizontal, 16)
            .padding(.vertical, 10)
            .foregroundColor(.black)
            .font(.body)
            .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.white))
    }
}

struct DetailForm_Previews: PreviewProvider {
    static var previews: some View {
       
             let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return   ActivityDetailForm(activity: Activity.createPreviewData(in: context)[0] ).environment(\.managedObjectContext, context).environment(\.colorScheme, .light)
            .navigationBarTitle("", displayMode: .inline)
        
    }
}

