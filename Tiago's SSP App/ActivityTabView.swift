//
//  ActivityTabView.swift
//  ActivityWallet
//
//  Created by Fabrizio Casaburi on 11/09/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import SwiftUI

struct ActivityTabView: View {
    var body: some View {
        TabView {
            ActivityList()
                .tabItem {
                    Image(systemName: "bookmark.fill")
                    Text("My Activities")
            }
            ActivitySchedule()
                .tabItem {
                    Image(systemName: "calendar")
                    Text("Schedule")
            }
        }
    }
}

struct ActivityTabView_Previews: PreviewProvider {
    static var previews: some View {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return
            ActivityTabView().environment(\.managedObjectContext, context)
    }
}
